#![feature(test)]

#[cfg(feature="race-leveldb")]
mod bench_leveldb {
extern crate leveldb;
extern crate tempdir;
extern crate test;

use self::tempdir::TempDir;
use self::leveldb::database::Database;
use self::leveldb::kv::KV;
use self::leveldb::options::{Options,WriteOptions,ReadOptions};

#[bench]
fn bench_leveldb_write_int(b: &mut test::Bencher) {
    let tempdir = TempDir::new("leveldb-bencher").unwrap();
    let path = tempdir.path();
     
    let mut options = Options::new();
    options.create_if_missing = true;
    let database = Database::open(path, options).unwrap();

    let write_opts = WriteOptions::new();
    b.iter(|| {
        database.put(write_opts, 1, &[1]).expect("Failed to write to database");
    })
}

#[bench]
fn bench_leveldb_read_int(b: &mut test::Bencher) {
    let tempdir = TempDir::new("leveldb-bencher").unwrap();
    let path = tempdir.path();
     
    let mut options = Options::new();
    options.create_if_missing = true;
    let database = Database::open(path, options).unwrap();

    let write_opts = WriteOptions::new();
    database.put(write_opts, 1, &[1]).expect("Failed to write to database");

    b.iter(|| {
        let read_opts = ReadOptions::new();
        let res = database.get(read_opts, 1);
    })
}

/*
#[bench]
fn bench_leveldb_read_string(b: &mut test::Bencher) {
    let tempdir = TempDir::new("leveldb-bencher").unwrap();
    let path = tempdir.path();
     
    let mut options = Options::new();
    options.create_if_missing = true;
    let database = Database::open(path, options).unwrap();

    let write_opts = WriteOptions::new();
    let key = b"This key is longer than a simple integer";
    database.put(write_opts, key, b"my value").expect("Failed to write to database");

    b.iter(|| {
        let read_opts = ReadOptions::new();
        let res = database.get(read_opts, key);
    })
}
*/
}

#[cfg(feature="race-rocksdb")]
mod bench_rocksdb {
extern crate test;
extern crate rocksdb;
extern crate tempdir;

use self::rocksdb::{DB, Writable};
use self::tempdir::TempDir;

#[bench]
fn bench_rocksdb_write_int(b: &mut test::Bencher) {
    let tempdir = TempDir::new("rocksdb-bencher").unwrap();
    let path = tempdir.path();
    let db = DB::open_default(path.to_str().unwrap()).unwrap();
    b.iter(|| {
        db.put(&[1], &[1]);
    })
}

#[bench]
fn bench_rocksdb_read_int(b: &mut test::Bencher) {
    let tempdir = TempDir::new("rocksdb-bencher").unwrap();
    let path = tempdir.path();
    let db = DB::open_default(path.to_str().unwrap()).unwrap();
    //db.put(b"my key", b"my value");
    db.put(&[1], &[1]);
    b.iter(|| {
        db.get(&[1]);
    })
}

#[bench]
fn bench_rocksdb_read_string(b: &mut test::Bencher) {
    let tempdir = TempDir::new("rocksdb-bencher").unwrap();
    let path = tempdir.path();
    let db = DB::open_default(path.to_str().unwrap()).unwrap();
    let key = b"This key is longer than a simple integer";
    db.put(key, b"my value");
    b.iter(|| {
        db.get(key);
    })
}
}

#[cfg(feature="race-tokyocabinet")]
mod bench_tokyocabinet {
extern crate test;
extern crate db_game3;
extern crate tempdir;

use std::path::PathBuf;
use self::tempdir::TempDir;
use self::db_game3::keyvalue::*;
use self::db_game3::tokyocabinet::*;

#[bench]
fn bench_tokyocabinet_hdb_read_string(b: &mut test::Bencher) {
    let tempdir = TempDir::new("tokyocabinet-bencher").unwrap();
    let mut path = PathBuf::new();
	path.push(tempdir.path());
	path.push("hdb-read-string.tch");
    let db = TokyoCabinetHashDB::new(path.to_str().unwrap()).unwrap();
    let key = b"This key is longer than a simple integer";
    db.put(key, b"my value");
    b.iter(|| {
        db.get(key);
    })
}
}

#[cfg(feature="race-kyotocabinet")]
mod bench_kyotocabinet {
extern crate test;
extern crate db_game3;
extern crate tempdir;

use self::db_game3::kyotocabinet::kc_banana;

#[bench]
fn bench_kyotocabinet_banana(b: &mut test::Bencher) {
    b.iter(|| {
        kc_banana();
    })
}
}
