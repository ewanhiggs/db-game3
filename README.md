# DB Game 3

This is the third iteration of my db game. 

This benchmarks a fer key value stores to make sure they're largely in line with
one another in terms of performance. As usual, my profiling is rubbish at the
fine level so we're only interested in orders of magnitude differences for
similar activities.

TODOs:

- [ ] Interesting Tests
- [x] LevelDB
- [x] RocksDB
- [ ] Tokyo Cabinet
- [ ]Kyoto Cabinet
- [ ]Berkeley DB
- [ ]GDBM
- [ ]Hash tables (a baseline)

## Building
To run the benchmarks, use `cargo bench`.

If you'd like to run a subset of the benchmarks, use the features mechanism in
Cargo: `cargo bench --features='leveldb rocksdb'`

If you have libraries installed in places that aren't in your `/usr/lib` or
similar places, prefix `cargo` with `LIBARY_PATH=/path/to/libraries`. For
example, I use: `LIBRARY_PATH=/home/ehiggs/.local/lib cargo bench
--features='rocksdb'`