use std::ops::Deref;
use std::slice;
use std::str::from_utf8;
use libc::{c_void, size_t};
use libc;


// Taken from Rocksdb: https://github.com/spacejam/rust-rocksdb/blob/master/src/rocksdb.rs
pub struct DBVector {
    base: *mut u8,
    len: usize,
}

impl Deref for DBVector {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe { slice::from_raw_parts(self.base, self.len) }
    }
}

impl Drop for DBVector {
    fn drop(&mut self) {
        unsafe {
            libc::free(self.base as *mut libc::c_void);
        }
    }
}

impl DBVector {
    pub fn from_c(val: *mut u8, val_len: size_t) -> DBVector {
        DBVector {
            base: val,
            len: val_len as usize,
        }
    }

    pub fn to_utf8<'a>(&'a self) -> Option<&'a str> {
        from_utf8(self.deref()).ok()
    }
}

pub trait KeyValueReader {
    fn get(&self, key: &[u8]) -> Option<DBVector>;
}

pub trait KeyValueWriter {
    fn put(&self, key: &[u8], value: &[u8]);
    fn del(&self, key: &[u8]);
}
