extern crate tokyocabinet_sys;

use keyvalue::{KeyValueReader, KeyValueWriter, DBVector};
use self::tokyocabinet_sys::{tchdb, tcbdb};
use std::slice;
use libc::{c_int, size_t};
use std::io::{Error, ErrorKind};
use std::ffi::{CString, CStr};
pub struct TokyoCabinetHashDB(tchdb::TCHDB);

impl TokyoCabinetHashDB {
    pub fn new(filename: &str) -> Result<TokyoCabinetHashDB, Error> {
        unsafe {
            let db = tchdb::tchdbnew();
            let cpath = CString::new(filename).unwrap();                        
            if !tchdb::tchdbopen(db, cpath.as_ptr(), (tchdb::HDBOWRITER | tchdb::HDBOCREAT).bits()) {
                let ecode = tchdb::tchdbecode(db);                                     
                let errmsg = tchdb::tchdberrmsg(ecode);                                
				return Err(Error::new(ErrorKind::Other, CStr::from_ptr(errmsg).to_string_lossy().into_owned()));
            }                    
			Ok(TokyoCabinetHashDB(db))
        }
    }
}

impl KeyValueReader for TokyoCabinetHashDB {
    fn get(&self, key: &[u8]) -> Option<DBVector> {
        unsafe {
            let mut val_sz: *const i32 = 0 as *const i32;
            let val_sz_ptr: *mut *const i32 = &mut val_sz;
            let val = tchdb::tchdbget(self.0, key.as_ptr(), key.len() as i32, val_sz_ptr);
            if val.is_null() {
                return None;
            }
            else{
                return Some(DBVector::from_c(val, val_sz as usize));
            }
        }
    }
}

impl KeyValueWriter for TokyoCabinetHashDB {
    fn put(&self, key: &[u8], value: &[u8]) {
        unsafe {
            if !tchdb::tchdbput(self.0, key.clone().as_ptr(), key.len() as c_int, value.clone().as_ptr(), value.len() as c_int) {
                // nothing
                return;
            }
        }
    }
    fn del(&self, key: &[u8]) {
        unsafe {
            if !tchdb::tchdbout(self.0, key.as_ptr(), key.len() as i32) {
                return;
            }
        }
    }
}
