extern crate libc;

pub mod keyvalue;

#[cfg(feature="race-leveldb")]
pub mod leveldb;

#[cfg(feature="race-rocksdb")]
pub mod rocksdb;

#[cfg(feature="race-tokyocabinet")]
pub mod tokyocabinet;

#[cfg(feature="race-kyotocabinet")]
pub mod kyotocabinet;
